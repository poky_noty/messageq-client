class KeyNames(object):
    _hostsup = "monitoring:discovery:hostsup"
    _openports = "monitoring:scan:openports"

    @property
    def hostsup(self):
        return self._hostsup

    @property
    def openports(self):
        return self._openports