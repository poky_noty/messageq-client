import redis 

from . import *
#from ..contact_message_queue import logger 

#import os
#host = os.environ["MESSAGE-QUEUE-HOST"]
#port = os.environ["MESSAGE-QUEUE-PORT"]
#db   = os.environ["MESSAGE-QUEUE-DB"]
#import configparser
#config = configparser.ConfigParser()

# /etc/messageqclient/messageqclient.cfg
#config.read("/etc/messageqclient/messageqclient.cfg")
# ***************************************************

#host = config.get("DEFAULT", "mqHost")
#port = config.get("DEFAULT", "mqPort")
#db = config.get("DEFAULT", "mqDB")

def establish_single_connection( logger, host, port, db ):
    logger.debug("Trying to establish a network connection with Redis")
    client = redis.StrictRedis(host=host, port=port, db=db)
    logger.debug("Successful Connection & Client is %s", client)
    
    # The test
    ping = client.ping()
    logger.debug("Successful Test & Ping is %s", ping)
    logger.debug("Redis Server Info is ['Version']=%s", client.info().get("redis_version"))

    return client

def establish_pool_connection():
    pass
