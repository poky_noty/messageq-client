from .simple_connection import establish_single_connection
from .simple_set_key_value import set_single_key_value, get_single_key_value
from .simple_exists_stored_key import exists_stored_key

from . import *
#from messageqclient.contact_message_queue import logger 

def is_redis_available_for_data( logger, host, port, db ):
    # The connection
    client = establish_single_connection( logger, host, port, db )
    logger.debug("Successful Connection & Client is %s", client)
    
    # The test
    ping = client.ping()
    logger.debug("Successful Test & Ping is %s", ping)

# **** Total Redis Server Info ****
#'redis_version': '5.0.4', 'redis_git_sha1': 0, 'redis_git_dirty': 0, 'redis_build_id'
#'e0bf1ed947563972', 'redis_mode': 'standalone', 'os': 'Linux 4.15.0-47-generic x86_64', #'arch_bits': 64, 'multiplexing_api': 'epoll', 'atomicvar_api': 'atomic-builtin', '
#gcc_version': '8.3.0', 'process_id': 1, 'run_id': '2d873d1a3942dd619f3bd9ff1bcd8a921c053b54', #'tcp_port': 6379, 'uptime_in_seconds': 742, 'uptime_in_days': 0, 'hz': 10, 'co
#nfigured_hz': 10, 'lru_clock': 15624438, 'executable': '/data/redis-server', 'config_file': '', #'connected_clients': 1, 'client_recent_max_input_buffer': 0, 'client_recent_
#max_output_buffer': 0, 'blocked_clients': 0, 'used_memory': 575368, 'used_memory_human': '561.88K', #'used_memory_rss': 2285568, 'used_memory_rss_human': '2.18M', 'used_memo
#ry_peak': 575368, 'used_memory_peak_human': '561.88K', 'used_memory_peak_perc': '112.27%', #'used_memory_overhead': 562166, 'used_memory_startup': 512472, 'used_memory_datas
#et': 13202, 'used_memory_dataset_perc': '20.99%', 'allocator_allocated': 662328, #'allocator_active': 823296, 'allocator_resident': 3186688, 'total_system_memory': 103305625
#6, 'total_system_memory_human': '985.20M', 'used_memory_lua': 37888, 'used_memory_lua_human': #'37.00K', 'used_memory_scripts': 0, 'used_memory_scripts_human': '0B', 'number
#_of_cached_scripts': 0, 'maxmemory': 0, 'maxmemory_human': '0B', 'maxmemory_policy': 'noeviction', #'allocator_frag_ratio': 1.24, 'allocator_frag_bytes': 160968, 'allocator_
#rss_ratio': 3.87, 'allocator_rss_bytes': 2363392, 'rss_overhead_ratio': 0.72, 'rss_overhead_bytes': #-901120, 'mem_fragmentation_ratio': 4.46, 'mem_fragmentation_bytes': 177
#3096, 'mem_not_counted_for_evict': 0, 'mem_replication_backlog': 0, 'mem_clients_slaves': 0, #'mem_clients_normal': 49694, 'mem_aof_buffer': 0, 'mem_allocator': 'jemalloc-5.
#1.0', 'active_defrag_running': 0, 'lazyfree_pending_objects': 0, 'loading': 0, #'rdb_changes_since_last_save': 0, 'rdb_bgsave_in_progress': 0, 'rdb_last_save_time': 15591275
#68, 'rdb_last_bgsave_status': 'ok', 'rdb_last_bgsave_time_sec': -1, 'rdb_current_bgsave_time_sec': #-1, 'rdb_last_cow_size': 0, 'aof_enabled': 0, 'aof_rewrite_in_progress':
#0, 'aof_rewrite_scheduled': 0, 'aof_last_rewrite_time_sec': -1, 'aof_current_rewrite_time_sec': -1, #'aof_last_bgrewrite_status': 'ok', 'aof_last_write_status': 'ok', 'aof_l
#ast_cow_size': 0, 'total_connections_received': 1, 'total_commands_processed': 2, #'instantaneous_ops_per_sec': 0, 'total_net_input_bytes': 51, 'total_net_output_bytes': 12,
#'instantaneous_input_kbps': 0.0, 'instantaneous_output_kbps': 0.0, 'rejected_connections': 0, #'sync_full': 0, 'sync_partial_ok': 0, 'sync_partial_err': 0, 'expired_keys':
#0, 'expired_stale_perc': 0.0, 'expired_time_cap_reached_count': 0, 'evicted_keys': 0, #'keyspace_hits': 0, 'keyspace_misses': 0, 'pubsub_channels': 0, 'pubsub_patterns': 0,
#'latest_fork_usec': 0, 'migrate_cached_sockets': 0, 'slave_expires_tracked_keys': 0, #'active_defrag_hits': 0, 'active_defrag_misses': 0, 'active_defrag_key_hits': 0, 'activ
#e_defrag_key_misses': 0, 'role': 'master', 'connected_slaves': 0, 'master_replid': #'6cea9d1ccbf8cabfdd3171353e02f5fe06123c85', 'master_replid2': 0, 'master_repl_offset': 0,
#'second_repl_offset': -1, 'repl_backlog_active': 0, 'repl_backlog_size': 1048576, #'repl_backlog_first_byte_offset': 0, 'repl_backlog_histlen': 0, 'used_cpu_sys': 1.340492,
#'used_cpu_user': 1.144671, 'used_cpu_sys_children': 0.006607, 'used_cpu_user_children': 0.005215, #'cluster_enabled': 0}
    logger.debug("Redis Server Info is ['Version']=%s", client.info().get("redis_version"))

    # Finished Simple Test ... OK
    return client

def try_single_key_value(client):
    # Use Default Values ... Dummy
    set_single_key_value(client)

    return

def try_set_get_key_value(client, key, value):
    # User-defined Values
    set_single_key_value(client=client, key=key, value=value)
    value_back = get_single_key_value(client=client, key=key)

    assert( value_back.decode("utf-8") == value)

def try_exists(client, key="dummy"):
    # Use Default Values .. Dummy/ Empty
    response = exists_stored_key(client=client, key=key)

    assert( response == True )
