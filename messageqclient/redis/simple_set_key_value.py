from . import *

def set_single_key_value(client, key="dummy", value="empty"):
    #logger.debug("Ready to Set a Single Key/ Value ... %s / %s", key, value)
    response = client.set(key, value)
    #logger.debug("Successful Set Response is %s", response)

    return response

def get_single_key_value(client, key):
    #logger.debug("Ready to Get a Single Key/ Value ... %s / ?", key)
    response = client.get( key )
    #logger.debug("Successful Get Response is %s", response)

    return response

