from setuptools import setup

install_requires = [
    "redis==3.2.1",
]

setup( 
    name='redis_client',
    version='1.0.0',
    description='Redis client to support the Local-Agent short-term storage requirements',
    long_description='Not given yet !',
    author='Miltos Vimplis',
    url='https://gitlab.com/poky_noty/messageq-client',
    license='MIT License',
    packages=['messageqclient', 'messageqclient.redis'],
    install_requires=install_requires,
    classifiers=[
        'Development Status :: 1 - Development/Testing',
        'Environment :: Module',
        'Programming Language :: Python :: 3.4',
        'Topic :: Storage :: Memory :: Key-Value :: Pub/Sub :: Listener :: Topic',
    ],
    #entry_points={
    #    'console_scripts': [
    #        'redis_client=messageqclient.__main__:main'
    #    ]
    #},
    zip_safe=True
)
